from setuptools import setup

from sigmatheta_gui.version import __version__


setup(name='sigmatheta-gui',
      version=__version__,
      scripts=['bin/SigmaTheta-gui'],
      packages=['sigmatheta_gui',
                'sigmatheta_gui.core',
                'sigmatheta_gui.ui',
                'sigmatheta_gui.data'],
      package_data={'sigmatheta_gui.data': ["data/*.png"]},
      install_requires=['st-binding',
                        'numpy',
                        'pyqtgraph',
                        'PyQt5'],
      download_url=['https://gitlab.com/sigmatheta1/sigmatheta_gui/'],
      author='Benoit Dubois',
      author_email='dubois.benoit@gmail.com',
      description='GUI for Sigma Theta library',
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Console',
          'Environment :: X11 Applications :: Qt',
          'Intended Audience :: End Users/Desktop',
          'License :: OSI Approved :: GNU General Public License (GPL) v3 or later (GPLv3+)',
          'Natural Language :: English',
          'Operating System :: POSIX',
          'Programming Language :: Python',
          'Topic :: Scientific/Engineering'],
      include_package_data=True,
      zip_safe=False)
