# -*- coding: utf-8 -*-
""" sigmatheta-gui/constants.py """

from PyQt5.QtCore import QDir


APP_NAME = "SigmaTheta-Gui"
APP_BRIEF = "GUI for Sigma Theta library."
AUTHOR_NAME = "Benoit Dubois"
AUTHOR_MAIL = "dubois.benoit@gmail.com"
COPYRIGHT = "Benoit Dubois, 2024"
LICENSE = "GNU GPL v3.0 or upper."

PLOT_LEGEND_FORMAT = ".3"

CSVIMPORT_MAX_LINE = 5  # Number of preview lines displayed in the table.
CSVIMPORT_MAX_PREVIEW_PTS = 1000  # Number of preview points displayed in the graph.
CSVIMPORT_DIRECTORY = QDir.homePath()  # File dialog opening directory
CSVIMPORT_COMMENT = "#"
CSVIMPORT_SEPARATOR = "\t"
CSVIMPORT_ENCODING = "utf_8"
