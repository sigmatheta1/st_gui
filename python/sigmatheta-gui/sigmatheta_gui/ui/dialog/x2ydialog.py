# -*- coding: utf-8 -*-

"""package sigmatheta-gui
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Dialog box to handle X2Y process request.
"""

from PyQt5 import QtWidgets, QtGui


class X2yDialog(QtWidgets.QDialog):
    """X2yDialog class, generate a dialog box used to get back scaling
    parameters to be applied to a time to frequency conversion.
    """

    def __init__(self, parent=None):
        """Constructor.
        :param parent: parent of widget (object)
        :returns: None
        """
        super().__init__(parent=parent)
        self.setWindowTitle("X2Y conversion")
        # Lays out
        self._xscale_led = QtWidgets.QLineEdit()
        self._xscale_led.setValidator(QtGui.QDoubleValidator(self._xscale_led))
        self._xscale_led.setText(str(1.0))
        self._yscale_led = QtWidgets.QLineEdit()
        self._yscale_led.setValidator(
            QtGui.QDoubleValidator(self._yscale_led))
        self._yscale_led.setText(str(1.0))
        self._btn_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok |
            QtWidgets.QDialogButtonBox.Cancel)
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(QtWidgets.QLabel("Timetag factor"))
        layout.addWidget(self._xscale_led)
        layout.addWidget(QtWidgets.QLabel("Phase factor"))
        layout.addWidget(self._yscale_led)
        layout.addWidget(self._btn_box)
        self.setLayout(layout)
        # Basic logic
        self._btn_box.accepted.connect(self.accept)
        self._btn_box.rejected.connect(self.close)

    @property
    def xscale(self):
        """Getter of the xscale factor value.
        :returns: xscale factor value (float)
        """
        return float(self._xscale_led.text())

    @property
    def yscale(self):
        """Getter of the yscale value.
        :returns: yscale value (float)
        """
        return float(self._yscale_led.text())