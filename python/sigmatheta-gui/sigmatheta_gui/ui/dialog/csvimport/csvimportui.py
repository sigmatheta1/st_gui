# -*- coding: utf-8 -*-

"""package sigmatheta-gui
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     TEXT/CSV (column data file) import UI.
"""

import logging
import numpy as np
from PyQt5.QtWidgets import (
    QDialog,
    QFileDialog,
    QGroupBox,
    QGridLayout,
    QTableView,
    QPushButton,
    QLabel,
    QDialogButtonBox,
    QLineEdit,
    QRadioButton,
    QMessageBox,
    QComboBox,
    QListView,
    QCheckBox,
    QButtonGroup,
)
from PyQt5.QtCore import (
    pyqtSlot,
    pyqtSignal,
    Qt,
    QDir,
    QAbstractTableModel,
    QStringListModel,
)
import pyqtgraph as pg
from . import ENCODINGS
from .myqbuttongroup import MyQButtonGroup


class CsvImportUi(QDialog):
    """A dialog widget dedicated to import text/csv type file."""

    def __init__(self, parent):
        super().__init__(parent)
        self._setup_ui()

    def _setup_ui(self):
        self.setWindowTitle("Text file import")
        #
        grid_layout = QGridLayout()
        grid_layout.setContentsMargins(11, 11, 11, 11)
        grid_layout.setSpacing(3)
        #
        open_gbox = QGroupBox("Import")
        open_layout = QGridLayout()
        self.open_btn = QPushButton("Open")
        self.file_lbl = QLabel()  ##self.filename)
        self.encoding_cbox = QComboBox()
        self.encoding_cbox.addItems(ENCODINGS)
        open_layout.addWidget(self.open_btn, 0, 0)
        open_layout.addWidget(self.file_lbl, 0, 1, 1, 2)
        open_layout.addWidget(QLabel("Character encoding"), 1, 0)
        open_layout.addWidget(self.encoding_cbox, 2, 0)
        open_layout.setColumnStretch(2, 1)
        open_gbox.setLayout(open_layout)
        #
        comment_gbox = QGroupBox("Comment character")
        comment_layout = QGridLayout()
        self.comment_btns = MyQButtonGroup()
        self.sharp_rbtn = QRadioButton("#")
        self.percent_rbtn = QRadioButton("%")
        self.comment_other_rbtn = QRadioButton("Other")
        self.comment_other_led = QLineEdit()
        self.comment_btns.addButton(self.sharp_rbtn, sid="#")
        self.comment_btns.addButton(self.percent_rbtn, sid="%")
        self.comment_btns.addButton(self.comment_other_rbtn, sid="Other")
        comment_layout.addWidget(self.sharp_rbtn, 0, 0)
        comment_layout.addWidget(self.percent_rbtn, 0, 1)
        comment_layout.addWidget(self.comment_other_rbtn, 0, 2)
        comment_layout.addWidget(self.comment_other_led, 0, 3)
        comment_gbox.setLayout(comment_layout)
        #
        sep_gbox = QGroupBox("Separator")
        sep_layout = QGridLayout()
        self.separator_btns = MyQButtonGroup()
        self.tab_rbtn = QRadioButton("Tab")
        self.comma_rbtn = QRadioButton("Comma")
        self.semicolon_rbtn = QRadioButton("Semicolon")
        self.space_rbtn = QRadioButton("Space")
        self.sep_other_rbtn = QRadioButton("Other")
        self.separator_btns.addButton(self.tab_rbtn, sid="\t")
        self.separator_btns.addButton(self.comma_rbtn, sid=",")
        self.separator_btns.addButton(self.semicolon_rbtn, sid=";")
        self.separator_btns.addButton(self.space_rbtn, sid=" ")
        self.separator_btns.addButton(self.sep_other_rbtn, sid="Other")
        self.sep_other_led = QLineEdit()
        sep_layout.addWidget(self.tab_rbtn, 0, 0)
        sep_layout.addWidget(self.comma_rbtn, 0, 1)
        sep_layout.addWidget(self.semicolon_rbtn, 0, 2)
        sep_layout.addWidget(self.space_rbtn, 0, 3)
        sep_layout.addWidget(self.sep_other_rbtn, 0, 4)
        sep_layout.addWidget(self.sep_other_led, 0, 5)
        sep_gbox.setLayout(sep_layout)
        #
        column_gbox = QGroupBox("Choose columns")
        column_layout = QGridLayout()
        self.column_t_cbox = QComboBox()
        self.column_y_cbox = QComboBox()
        tlabel = QLabel("Timetag")
        column_layout.addWidget(tlabel, 0, 0)
        column_layout.addWidget(self.column_t_cbox, 0, 1)
        column_layout.addWidget(QLabel("Data"), 0, 2)
        column_layout.addWidget(self.column_y_cbox, 0, 3)
        column_layout.setColumnStretch(1, 1)
        column_layout.setColumnStretch(3, 1)
        column_gbox.setLayout(column_layout)
        #
        rawview_gbox = QGroupBox("Raw view")
        self.file_view = QListView()
        rawview_layout = QGridLayout()
        rawview_layout.addWidget(self.file_view, 0, 0)
        rawview_gbox.setLayout(rawview_layout)
        #
        format_gbox = QGroupBox("Formated view")
        self.preview_ckbox = QCheckBox("Graphical preview")
        self.format_view = QTableView()
        self.preview_graph = pg.PlotWidget()
        self.preview_graph.getPlotItem().showAxes(True, False)
        format_layout = QGridLayout()
        format_layout.addWidget(self.preview_ckbox, 0, 0)
        format_layout.addWidget(self.format_view, 1, 0)
        format_layout.addWidget(self.preview_graph, 1, 1)
        format_layout.setColumnStretch(0, 2)
        format_layout.setColumnStretch(1, 2)
        format_gbox.setLayout(format_layout)
        #
        self.close_btns = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        )
        #
        row = 0
        grid_layout.addWidget(open_gbox, row, 0, 1, 2)
        row += 1
        grid_layout.addWidget(rawview_gbox, row, 0, 1, 2)
        row += 1
        grid_layout.addWidget(comment_gbox, row, 0)
        grid_layout.addWidget(
            sep_gbox,
            row,
            1,
        )
        row += 1
        grid_layout.addWidget(column_gbox, row, 0, 1, 2)
        row += 1
        grid_layout.addWidget(format_gbox, row, 0, 1, 2)
        row += 1
        grid_layout.addWidget(self.close_btns, row, 0, 1, 2)
        grid_layout.setColumnStretch(1, 3)
        self.setLayout(grid_layout)
