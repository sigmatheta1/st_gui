# -*- coding: utf-8 -*-

"""package sigmatheta-gui
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Model (Qt Model/View pattern) used to represent numpy data.
"""

from PyQt5 import Qt, QtCore


class NumpyModel(QtCore.QAbstractTableModel):
    """Model (Qt Model/View pattern) used to represent numpy data.
    """

    def __init__(self, narray, parent=None):
        super().__init__(parent=parent)
        self._array = narray

    def rowCount(self, parent=None):
        return self._array.shape[0]

    def columnCount(self, parent=None):
        if len(self._array.shape) == 1:
            return 1
        return self._array.shape[1]

    def data(self, index, role=Qt.Qt.DisplayRole):
        if index.isValid():
            if role == Qt.Qt.DisplayRole:
                if self.columnCount() == 1:
                    row = index.row()
                    return str(self._array[row])
                row = index.row()
                col = index.column()
                return str(self._array[row, col])
