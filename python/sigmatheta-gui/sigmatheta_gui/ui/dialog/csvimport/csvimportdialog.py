# -*- coding: utf-8 -*-

"""package sigmatheta-gui
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     TEXT/CSV (column data file) import dialog gui.
"""

import io
import logging
import numpy as np
from PyQt5.QtWidgets import QDialogButtonBox, QFileDialog
from PyQt5.QtCore import (
    pyqtSlot,
    pyqtSignal,
    Qt,
    QDir,
    QAbstractTableModel,
    QStringListModel,
    QObject,
)
import pyqtgraph as pg
from .numpymodel import NumpyModel
from .csvimportui import CsvImportUi
from . import *


# ## Switch to using white background and black foreground
## pg.setConfigOption("background", "w")
## pg.setConfigOption("foreground", "k")


class CsvImportDialog(QObject):
    """A dialog widget dedicated to import text/csv type file."""

    comment_changed = pyqtSignal(str)
    separator_changed = pyqtSignal(str)

    def __init__(
        self,
        parent=None,
        directory=DEFAULT_DIRECTORY,
        comment=DEFAULT_COMMENT,
        separator=DEFAULT_SEPARATOR,
        encoding=DEFAULT_ENCODING,
        xcol_enable=True,
    ):
        super().__init__(parent)
        if encoding not in ENCODINGS:
            raise ValueError("Illegal encoding parameter")
        self.directory = directory
        self.encoding = encoding
        self._xcol_enable = xcol_enable
        self.timetag_column = 0
        self.data_column = 1
        self.filename = ""
        self.preview_graph = None
        self.comment = comment
        self.separator = separator
        self._ui = CsvImportUi(parent)
        self._ui.setWindowTitle("Import data")
        # if self._xcol_enable is False:
        #     tlabel.setEnabled(False)
        #     self._column_t_cbox.setEnabled(False)
        #
        self._ui.encoding_cbox.setCurrentIndex(
            self._ui.encoding_cbox.findText(encoding)
        )
        self._ui.comment_btns.button(comment).setChecked(True)
        self._ui.separator_btns.button(separator).setChecked(True)
        #
        self._ui.open_btn.released.connect(self._open_triggered)
        self._ui.encoding_cbox.currentIndexChanged[str].connect(self._set_encoding)
        #
        self._ui.sharp_rbtn.toggled.connect(lambda: self._set_comment("#"))
        self._ui.percent_rbtn.toggled.connect(lambda: self._set_comment("%"))
        self._ui.comment_other_rbtn.toggled.connect(
            lambda: self._set_comment(self._ui.comment_other_led.text())
        )
        self._ui.comment_other_led.textChanged.connect(
            lambda: self.set_comment(self._ui.comment_other_led.text())
        )
        self._ui.comment_other_rbtn.toggled.connect(
            self._ui.comment_other_led.setEnabled
        )
        #
        self._ui.tab_rbtn.toggled.connect(lambda: self._set_separator("\t"))
        self._ui.comma_rbtn.toggled.connect(lambda: self._set_separator(","))
        self._ui.semicolon_rbtn.toggled.connect(lambda: self._set_separator(";"))
        self._ui.space_rbtn.toggled.connect(lambda: self._set_separator(" "))
        self._ui.sep_other_rbtn.toggled.connect(
            lambda: self._set_separator(self._ui.sep_other_led.text())
        )
        self._ui.sep_other_led.textChanged.connect(
            lambda: self._set_separator(self._ui.sep_other_led.text())
        )
        self._ui.sep_other_rbtn.toggled.connect(self._ui.sep_other_led.setEnabled)
        #
        if self._xcol_enable is True:
            self._ui.column_t_cbox.currentIndexChanged[str].connect(self._set_t_column)
        self._ui.column_y_cbox.currentIndexChanged[str].connect(self._set_y_column)
        #
        self.separator_changed.connect(self._update)
        self.comment_changed.connect(self._update)
        #
        self._ui.preview_ckbox.stateChanged.connect(self._update_preview)
        self._ui.column_t_cbox.currentIndexChanged.connect(self._update_preview)
        self._ui.column_y_cbox.currentIndexChanged.connect(self._update_preview)
        self.separator_changed.connect(self._update_preview)
        self.comment_changed.connect(self._update_preview)
        #
        self._ui.close_btns.button(QDialogButtonBox.Ok).clicked.connect(self._ui.accept)
        self._ui.close_btns.button(QDialogButtonBox.Cancel).clicked.connect(self._ui.reject)
        #
        self._update()

    def exec(self):
        return self._ui.exec()

    def show(self):
        return self._ui.show()

    def _set_t_column(self, t):
        self.timetag_column = t

    def _set_y_column(self, y):
        self.data_column = y

    def _set_encoding(self, encoding):
        self.encoding = encoding

    def _set_comment(self, comment):
        self.comment = comment
        self.comment_changed.emit(comment)

    def _set_separator(self, separator):
        self.separator = separator
        self.separator_changed.emit(separator)

    def _set_filename(self, filename):
        self.filename = filename
        self._ui.file_lbl.setText(filename)

    @pyqtSlot()
    def _update_preview(self):
        if self.filename == "":
            return
        if self._ui.preview_ckbox.isChecked() is True:
            self._ui.preview_graph.clear()
            try:
                self.plot_preview()
            except TypeError:
                logging.info("File data type error")
            except UnicodeDecodeError:
                logging.info("File bad character encoding selected, choose another one")
            except ValueError as ex:
                logging.warning("Parsing file problem")
                logging.debug("Parsing file problem: %r", ex)
        else:
            self._ui.preview_graph.clear()

    def plot_preview(self):
        # Read file
        _encoding = self._ui.encoding_cbox.currentText()
        # Problem with open()??
        # with open(self.filename, encoding=_encoding) as fd:
        data = np.genfromtxt(
            self.filename,
            delimiter=self.separator,
            comments=self.comment,
            encoding=_encoding,
        )
        data = np.transpose(data)
        # Use only y data for displaying preview.
        data = np.array(data[int(self._ui.column_y_cbox.currentText()) - 1])
        if len(data) > MAX_PREVIEW_PTS:
            ds = int(len(data) / MAX_PREVIEW_PTS)
            self._ui.preview_graph.setDownsampling(ds=ds, mode="peak")
        self._ui.preview_graph.plot(data)

    @pyqtSlot()
    def _open_triggered(self):
        """ """
        filename, filter = QFileDialog.getOpenFileName(
            self._ui, "Import texte file", self.directory
        )
        if filename != "":
            self._set_filename(filename)
            self._update()

    @pyqtSlot()
    def _update(self):
        if self.filename == "":
            return False
        # Read file
        _encoding = self._ui.encoding_cbox.currentText()
        try:
            with open(self.filename, encoding=_encoding) as fd:
                i = 0
                lines = ""
                # Read MAX_LINE lines of data (not lines of comments!)
                while i < MAX_LINE:
                    line = fd.readline()
                    if line[0] != self.comment:
                        lines += line
                        i += 1
        except UnicodeDecodeError:
            QMessageBox.warning(
                self._ui,
                "Encoding problem",
                "Bad character encoding selected, choose another one",
            )
            return False
        # Update raw view
        rmodel = QStringListModel([line for line in lines.split("\n")])
        self._ui.file_view.setModel(rmodel)
        # Parse lines
        try:
            data = np.genfromtxt(
                io.BytesIO(lines.encode()),
                delimiter=self.separator,
                comments=self.comment,
            )
        except ValueError as ex:
            logging.warning("Parsing file problem: %s", ex)
            return False
        # Update formated (data) view
        fmodel = NumpyModel(data)
        self._ui.format_view.setModel(fmodel)
        #
        self._ui.column_t_cbox.clear()
        self._ui.column_y_cbox.clear()
        try:
            self._ui.column_t_cbox.addItem(" ")
            for col in range(fmodel.columnCount()):
                self._ui.column_t_cbox.addItem(str(col + 1))
                self._ui.column_y_cbox.addItem(str(col + 1))
        except IndexError as ex:
            logging.debug(ex)
        if fmodel.columnCount() == 1:
            self._ui.column_t_cbox.setCurrentIndex(0)
            self._ui.column_y_cbox.setCurrentIndex(0)
        else:
            self._ui.column_t_cbox.setCurrentIndex(1)
            self._ui.column_y_cbox.setCurrentIndex(1)
        return True


# ----------------------------------------------------------------------------
if __name__ == "__main__":
    # Chek import dialog UI
    import sys
    from PyQt5.QtWidgets import QApplication

    APP = QApplication(sys.argv)
    DIALOG = CsvImportDialog(directory="../", encoding="latin_1", xcol_enable=True)
    DIALOG.show()
    sys.exit(APP.exec_())
