# -*- coding: utf-8 -*-

"""package sigmatheta-gui
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     QButtonGroup class with string identifier.
"""

from functools import singledispatchmethod
from PyQt5 import QtWidgets


class MyQButtonGroup(QtWidgets.QButtonGroup):
    """QButtonGroup class with string identifier.
    """
    
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.sid = {}  # "string id" dict

    def addButton(self, button, id=-1, sid=None):
        super().addButton(button, id)
        if sid is None or sid in self.sid.keys():
            # use (automaticaly) assigned id if sid not defined or already assigned
            self.sid[self.id(button)] = button
        else:
            self.sid[sid] = button

    def sid(self, button):
        """Returns the string id of the specified button, or -1 if no such button
        exists.
        """
        if button not in self.buttons():
            return -1
        return list(self.sid)[list(self.sid.values()).index(button)]

    @singledispatchmethod
    def button(self, idn):
        raise NotImplementedError("Bad usage of button() method.")

    @button.register
    def _(self, idn: int):
        return super().button(idn)

    @button.register
    def _(self, idn: str):
        return self.sid[idn]