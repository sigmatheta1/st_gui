# -*- coding: utf-8 -*-

"""package sigmatheta-gui
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Dialog box to handle scaling process request.
"""

from PyQt5 import QtWidgets, QtGui


class ScalingDialog(QtWidgets.QDialog):
    """ScalingDialog class, generate a dialog box used to get back scaling
    parameters to be applied to a curve.
    """

    def __init__(self, parent=None):
        """Constructor.
        :param parent: parent of widget (object)
        :returns: None
        """
        super().__init__(parent=parent)
        self.setWindowTitle("Data scaling")
        # Lays out
        self._scale_led = QtWidgets.QLineEdit()
        self._scale_led.setValidator(QtGui.QDoubleValidator(self._scale_led))
        self._scale_led.setText(str(1.0))
        self._offset_led = QtWidgets.QLineEdit()
        self._offset_led.setValidator(
            QtGui.QDoubleValidator(self._offset_led))
        self._offset_led.setText(str(0.0))
        self._btn_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok |
            QtWidgets.QDialogButtonBox.Cancel)
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(QtWidgets.QLabel("Multiplier factor"))
        layout.addWidget(self._scale_led)
        layout.addWidget(QtWidgets.QLabel("Sum factor"))
        layout.addWidget(self._offset_led)
        layout.addWidget(self._btn_box)
        self.setLayout(layout)
        # Basic logic
        self._btn_box.accepted.connect(self.accept)
        self._btn_box.rejected.connect(self.close)

    @property
    def scale(self):
        """Getter of the scale factor value.
        :returns: scale factor value (float)
        """
        return float(self._scale_led.text())

    @property
    def offset(self):
        """Getter of the offset value.
        :returns: offset value (float)
        """
        return float(self._offset_led.text())