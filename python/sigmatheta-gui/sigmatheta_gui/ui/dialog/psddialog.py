# -*- coding: utf-8 -*-

"""package sigmatheta-gui
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Dialog box to handle PSD process request.
"""

from PyQt5 import QtWidgets



periodogram_params = [
    {'name': 'fs', 'type': 'float', 'value': 1.0},
    {'name': 'window',
     'type': 'list',
     'limits': ['boxcar',
                'triang',
                'blackman',
                'hamming',
                'hann',
                'hanning',
                'bartlett',
                'flattop',
                'parzen',
                'bohman',
                'blackmanharris'],
     'value': 'boxcar'},
    {'name': 'nfft', 'type': 'int', 'limits': (1, 32768), 'value': None},
    #{'name': 'detrend'},
    #{'name': 'return_onesided', 'type': 'bool', 'value': rue},
    {'name': 'scaling', 'type': 'list',
     'limits': ['density', 'spectrum'], 'value': 'density'}
]

welch_params = [
    {'name': 'fs', 'type': 'float', 'value': 1.0},
    {'name': 'window', 'type': 'list', 'values':['boxcar', 'triang', 'blackman', 'hamming', 'hann', 'hanning', 'bartlett', 'flattop', 'parzen', 'bohman', 'blackmanharris'], 'value': 'hanning'},
    {'name': 'nperseg', 'type': 'int', 'limits': (1, 32768), 'value': 256},
    {'name': 'noverlap', 'type': 'int', 'limits': (1, 32768), 'value': None},
    {'name': 'nfft', 'type': 'int', 'limits': (1, 32768), 'value':None},
    #{'name': 'detrend'},
    #{'name': 'return_onesided', 'type': 'bool', 'value': True},
    {'name': 'scaling', 'type': 'list', 'values': ['density', 'spectrum'], 'value': 'density'}
]

psd_algorithm = ['Periodogram', 'Welch']

psd_algo_params = [periodogram_params, welch_params]

psd_params = [
    {'name': 'Frequency analysis', 'type': 'group', 'children': [
        {'name': 'Algorithm', 'type': 'list', 'limits': psd_algorithm, 'value': 0},
        {'name': 'Frequency analysis', 'type': 'group', 'children':[
            psd_algo_params[0]
        ]},
        {'name': 'Process analysis', 'type': 'action'},
    ]}
]


class PsdDialog(QtWidgets.QDialog):
    """PsdDialog class, generate a dialog box used to get back PSD parameters.
    """

    def __init__(self, parent=None):
        """Constructor.
        :param parent: parent of widget (object)
        :returns: None
        """
        super().__init__(parent=parent)
        self.setWindowTitle("PSD")
        # Lays out
        # self._drift_order_led = QtWidgets.QLineEdit()
        # self._drift_order_led.setValidator(QtGui.QIntValidator(
        #     self._drift_order_led))
        # self._drift_order_led.setText(str(self.DEFAULT_DRIFT_REMOVE_ORDER))
        self._btn_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok |
            QtWidgets.QDialogButtonBox.Cancel)
        layout = QtWidgets.QVBoxLayout()
        # layout.addWidget(QtWidgets.QLabel("Drift degree"))
        # layout.addWidget(self._drift_order_led)
        layout.addWidget(self._btn_box)
        self.setLayout(layout)
        # Basic logic
        self._btn_box.accepted.connect(self.accept)
        self._btn_box.rejected.connect(self.close)
