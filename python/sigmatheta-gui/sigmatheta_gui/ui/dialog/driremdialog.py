# -*- coding: utf-8 -*-

"""package sigmatheta-gui
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Dialog box to handle drift remove process request.
"""

from PyQt5 import QtWidgets, QtGui


class DriRemDialog(QtWidgets.QDialog):
    """DriRemDialog class, generate a dialog box used to get back drift remove
    parameters to be applied to a curve.
    """

    # TODO: add preview

    DEFAULT_DRIFT_REMOVE_ORDER = 1

    def __init__(self, parent=None):
        """Constructor.
        :param parent: parent of widget (object)
        :returns: None
        """
        super().__init__(parent=parent)
        self.setWindowTitle("Drift remove scaling")
        # Lays out
        self._drift_order_led = QtWidgets.QLineEdit()
        self._drift_order_led.setValidator(QtGui.QIntValidator(
            self._drift_order_led))
        self._drift_order_led.setText(str(self.DEFAULT_DRIFT_REMOVE_ORDER))
        self._btn_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok |
            QtWidgets.QDialogButtonBox.Cancel)
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(QtWidgets.QLabel("Drift degree"))
        layout.addWidget(self._drift_order_led)
        layout.addWidget(self._btn_box)
        self.setLayout(layout)
        # Basic logic
        self._btn_box.accepted.connect(self.accept)
        self._btn_box.rejected.connect(self.close)

    @property
    def drift_order(self):
        """Getter of the drift order value.
        :returns: drift order value (int)
        """
        return int(self._drift_order_led.text())