# -*- coding: utf-8 -*-

"""package sigmatheta-gui
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Widget dedicated to display a graph of deviation serie.
"""

import numpy as np
import pyqtgraph as pg
from .myerrorbaritem import MyErrorBarItem

## Use white background and black foreground for plot
pg.setConfigOption("background", "w")
pg.setConfigOption("foreground", "k")
# Enable antialiasing for prettier plots
pg.setConfigOptions(antialias=True)


LEGEND_FORMAT = ".3"


class DevPlot(pg.PlotWidget):
    """The DevPlot class is dedicated to display a graph of a deviation serie.
    The class is derived from pyqtgraph.PlotWidget and customized for the
    specific needs of deviation serie display.
    """

    def __init__(
        self, parent=None, background="default", legend_format=LEGEND_FORMAT, **kargs
    ):
        """Constructor."""
        super().__init__(parent, background, **kargs)
        self.serie = None
        self.serie_unb = None
        self.asym = []
        self.legend = None
        self.legend_format = legend_format
        self._customize_plot()

    def _customize_plot(self):
        """Customize look of widget.
        :returns: None
        """
        self.setTitle("")  # Cosmetic: add space above plot
        self.setLabel("left", "&sigma;<sub>y</sub> (&tau;)")
        self.setLabel("bottom", "Integration time &tau;", units="s")
        self.setLogMode(x=True, y=True)
        self.enableAutoRange(pg.ViewBox.XYAxes, False)
        self.showGrid(x=True, y=True, alpha=0.8)
        self.getPlotItem().getAxis("bottom").enableAutoSIPrefix(False)
        self.getPlotItem().getAxis("left").enableAutoSIPrefix(False)
        # self.setClickable(False) ## Here?? ##

    def clear(self):
        super().clear()
        self.serie = None
        self.serie_unb = None
        self.asym = []
        self.legend = None

    def plot_dev(
        self,
        x=None,
        y=None,
        yunb=None,
        top1s=None,
        bottom1s=None,
        top2s=None,
        bottom2s=None,
    ):
        """Plot deviation serie on graph.
        :returns: None
        """
        if x is None:
            x = []
        if y is None:
            y = []
        self.enableAutoRange(pg.ViewBox.XYAxes, True)
        if top1s is not None and bottom1s is not None:
            err = MyErrorBarItem(
                x=x, y=y, top=top1s, bottom=bottom1s, beam=0.03, pen=1.5
            )
            err.setLogMode(True, True)
            self.addItem(err)
        if top2s is not None and bottom2s is not None:
            err = MyErrorBarItem(
                x=x, y=y, top=top2s, bottom=bottom2s, beam=0.06, pen=0.4
            )
            err.setLogMode(True, True)
            self.addItem(err)
        if yunb is not None:
            self.serie_unb = self.plot(x, yunb, symbol="+", pen=None, name="serie_unb")
        self.serie = self.plot(x, y, symbol="x", pen=None, name="serie")
        del self.legend
        self.legend = self.addLegend()
        self.enableAutoRange(pg.ViewBox.XYAxes, False)

    def plot_asymptotes(self, coeff, visible=False):
        x_range = [self.serie.xData[0], self.serie.xData[-1]]
        if coeff[0] != 0:
            ln = self._plot_formula(x_range, lambda x: np.sqrt(coeff[0] / x**3), 0)
            ln.opts["name"] = f"{np.sqrt(coeff[0]):{self.legend_format}} &tau;<sup>-3/2</sup>"
            self.asym.append(ln)
        if coeff[1] != 0:
            ln = self._plot_formula(x_range, lambda x: np.sqrt(coeff[1] / x**2), 1)
            ln.opts["name"] = f"{np.sqrt(coeff[1]):{self.legend_format}} &tau;<sup>-1</sup>"
            self.asym.append(ln)
        if coeff[2] != 0:
            ln = self._plot_formula(x_range, lambda x: np.sqrt(coeff[2] / x), 2)
            ln.opts["name"] = f"{np.sqrt(coeff[2]):{self.legend_format}} &tau;<sup>-1/2</sup>"
            self.asym.append(ln)
        if coeff[3] != 0:
            ln = self._plot_formula(x_range, lambda x: np.sqrt(coeff[3] * x / x), 3)
            ln.opts["name"] = f"{np.sqrt(coeff[3]):{self.legend_format}} &tau;<sup>0</sup>"
            self.asym.append(ln)
        if coeff[4] != 0:
            ln = self._plot_formula(x_range, lambda x: np.sqrt(coeff[4] * x), 4)
            ln.opts["name"] = f"{np.sqrt(coeff[4]):{self.legend_format}} &tau;<sup>1/2</sup>"
            self.asym.append(ln)
        if coeff[5] != 0:
            ln = self._plot_formula(x_range, lambda x: np.sqrt(coeff[5] * x**2), 5)
            ln.opts["name"] = f"{np.sqrt(coeff[5]):{self.legend_format}} &tau;<sup>1</sup>"
            self.asym.append(ln)
        self.set_asymptotes_visible(visible)

    def set_asymptotes_visible(self, visible=True):
        if visible is True:
            for line in self.asym:
                self.addItem(line)
        else:
            for line in self.asym:
                self.removeItem(line)
        del self.legend
        self.legend = self.addLegend()

    def _plot_formula(self, x_range, formula, order, nb_pts=10):
        """Plot a formula.
        :param formula: formula of curve to plot (lambda function)
        :param nb_pts: number of points to use to plot formula (int)
        :returns: None
        """
        x = np.logspace(np.log10(x_range[0]), np.log10(x_range[1]), nb_pts)
        return self.plot(x, formula(x), pen=(1, order * 1.33))
