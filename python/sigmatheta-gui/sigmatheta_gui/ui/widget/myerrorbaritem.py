# -*- coding: utf-8 -*-

"""package sigmatheta-gui
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Subclass pyqtgraph ErrorBarItem to add log mode display option.
"""

import numpy as np
import pyqtgraph as pg


class MyErrorBarItem(pg.ErrorBarItem):
    """Subclass ErrorBarItem to add log mode display of error bar.
    """

    # logx = None
    # logy = None

    def __init__(self, **opts):
        """Constructor.
        """
        super().__init__(**opts)
        self.opts['logx'] = False
        self.opts['logy'] = False

    def setLogMode(self, x, y):
        """ This will be called by the PlotItem when the error bar is first
        added and whenever the log scaling mode changes.
        """
        if x is True:
            self.opts['logx'] = True
        else:
            self.opts['logx'] = False
            ## self.logx = True
        if y is True:
            ##self.logy = True
            self.opts['logy'] = True
        else:
            self.opts['logy'] = False

    def drawPath(self):
        p = pg.QtGui.QPainterPath()
        x, y = self.opts['x'], self.opts['y']
        logx, logy = self.opts['logx'], self.opts['logy']
        if x is None or y is None:
            return
        beam = self.opts['beam']
        height, top, bottom = self.opts['height'], \
                              self.opts['top'], \
                              self.opts['bottom']
        ## if self.logx is True:
        if logx is True:
            x = np.log10(x)
        ## if self.logy is True:
        if logy is True:
            y = np.log10(y)
        if height is not None or top is not None or bottom is not None:
            # draw vertical error bars
            if height is not None:
                if logy is True:
                    y1 = np.log10(10**y - height/2.)
                    y2 = np.log10(10**y + height/2.)
                else:
                    y1 = y - height/2.
                    y2 = y + height/2.
            else:
                if bottom is None:
                    y1 = y
                else:
                    if logy is True:
                        y1 = np.log10(10**y - bottom)
                    else:
                        y1 = y - bottom
                if top is None:
                    y2 = y
                else:
                    if logy is True:
                        y2 = np.log10(10**y + top)
                    else:
                        y2 = y + top

            for i, x_i in enumerate(x):
                p.moveTo(x_i, y1[i])
                p.lineTo(x_i, y2[i])

            if beam is not None and beam > 0:
                x1 = x - beam/2.
                x2 = x + beam/2.
                if height is not None or top is not None:
                    for i in range(len(x)):
                        p.moveTo(x1[i], y2[i])
                        p.lineTo(x2[i], y2[i])
                if height is not None or bottom is not None:
                    for i in range(len(x)):
                        p.moveTo(x1[i], y1[i])
                        p.lineTo(x2[i], y1[i])

        width, right, left = self.opts['width'], \
                             self.opts['right'], \
                             self.opts['left']
        if width is not None or right is not None or left is not None:
            # draw vertical error bars
            if width is not None:
                x1 = x - width/2.
                x2 = x + width/2.
            else:
                if left is None:
                    x1 = x
                else:
                    x1 = x - left
                if right is None:
                    x2 = x
                else:
                    x2 = x + right

            for i in range(len(x)):
                p.moveTo(x1[i], y[i])
                p.lineTo(x2[i], y[i])

            if beam is not None and beam > 0:
                y1 = y - beam/2.
                y2 = y + beam/2.
                if width is not None or right is not None:
                    for i in range(len(x)):
                        p.moveTo(x2[i], y1[i])
                        p.lineTo(x2[i], y2[i])
                if width is not None or left is not None:
                    for i in range(len(x)):
                        p.moveTo(x1[i], y1[i])
                        p.lineTo(x1[i], y2[i])

        self.path = p
        self.prepareGeometryChange()
