# -*- coding: utf-8 -*-

"""package sigmatheta-gui
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Widget dedicated to display the result of a serie computation and
          the parameters of the serie.
"""

import numpy as np
from PyQt5 import QtWidgets
import pyqtgraph as pg
from pyqtgraph.parametertree import Parameter, ParameterTree
import st_binding as stb
from .devplot import DevPlot


param_dict = [
    {
        "name": "Temporal analysis",
        "type": "group",
        "children": [
            {"name": "Filename", "type": "str", "value": "", "readonly": True},
            {
                "name": "Deviance type",
                "type": "list",
                "limits": list(stb.DEVIANCE_TYPE.keys()),
                "value": list(stb.DEVIANCE_TYPE)[0],
            },
            {"name": "Integration time (s)", "type": "float", "value": 1.0},
            {
                "name": "Tau step",
                "type": "list",
                "limits": list(stb.TAU_INC_TYPE.keys()),
                "value": list(stb.TAU_INC_TYPE)[0],
            },
            {"name": "Number of points", "type": "int", "value": 0, "readonly": True},
            {"name": "Start", "type": "int", "limits": (1, 1), "value": 1},
            {"name": "Stop", "type": "int", "limits": (1, 1), "value": 1},
            {"name": "Display asymptote", "type": "bool", "value": False},
        ],
    }
]


class SerieWidget(QtWidgets.QWidget):
    """Widget dedicated to display the result of a serie computation and
    the parameters of the serie (deviance type, number of samples...).
    """

    def __init__(self, plot_legend_format):
        """Initialization."""
        super().__init__()
        self.plot_legend_format = plot_legend_format
        self._setup_ui()
        self.preview_rbtn.released.connect(
            lambda: self.results.setCurrentWidget(self.preview)
        )
        self.plot_rbtn.released.connect(
            lambda: self.results.setCurrentWidget(self.plot)
        )
        self.table_rbtn.released.connect(
            lambda: self.results.setCurrentWidget(self.table)
        )
        self.psd_rbtn.released.connect(lambda: self.results.setCurrentWidget(self.psd))
        self.params.param("Temporal analysis", "Start").sigValueChanged.connect(
            self._xrange_changed
        )
        self.params.param("Temporal analysis", "Stop").sigValueChanged.connect(
            self._xrange_changed
        )
        self.psd_rbtn.setVisible(False)
        self.psd.setVisible(False)
        self.preview_rbtn.setChecked(True)

    def _xrange_changed(self):
        start = self.params.param("Temporal analysis", "Start").value()
        stop = self.params.param("Temporal analysis", "Stop").value()
        self.params.param("Temporal analysis", "Number of points").setValue(
            stop - start + 1
        )

    def _setup_ui(self):
        """Generate UI.
        :returns: None
        """
        select = QtWidgets.QHBoxLayout()
        self.preview_rbtn = QtWidgets.QRadioButton("Preview")
        self.plot_rbtn = QtWidgets.QRadioButton("Plot serie")
        self.table_rbtn = QtWidgets.QRadioButton("Table serie")
        self.psd_rbtn = QtWidgets.QRadioButton("PSD")
        select.addWidget(self.preview_rbtn)
        select.addWidget(self.plot_rbtn)
        select.addWidget(self.table_rbtn)
        select.addWidget(self.psd_rbtn)
        self.results = QtWidgets.QStackedLayout()
        self.preview = pg.PlotWidget()
        self.preview.setTitle("")  # In order to add space above plot
        self.preview.setDownsampling(auto=True, mode="peak")
        self.preview.enableAutoRange(pg.ViewBox.XYAxes, True)
        self.plot = DevPlot(legend_format=self.plot_legend_format)
        self.table = pg.TableWidget(sortable=False)
        self.table.setFormat("%20.3e")  # Global formating
        self.table.setFormat("%29.1f", column=0)  # Specific time formating
        self.psd = pg.PlotWidget()
        self.psd.setTitle("")  # Cosmetic: add space above plot
        self.psd.setLabel("left", "PSD (dBHz<sup>2</sup>/Hz)")
        self.psd.setLabel("bottom", "Frequency (Hz)")
        self.psd.enableAutoRange(pg.ViewBox.XYAxes, True)
        self.psd.setLogMode(x=True, y=True)
        self.psd.showGrid(x=True, y=True, alpha=0.8)
        self.psd.getPlotItem().getAxis("bottom").enableAutoSIPrefix(False)
        self.psd.getPlotItem().getAxis("left").enableAutoSIPrefix(False)
        self.results.addWidget(self.preview)
        self.results.addWidget(self.plot)
        self.results.addWidget(self.table)
        self.results.addWidget(self.psd)
        self.params = Parameter.create(
            name="Serie parameters", type="group", children=param_dict
        )
        self.param_tree = ParameterTree()
        self.param_tree.setParameters(self.params, showTop=False)
        ui_layout = QtWidgets.QGridLayout()
        ui_layout.addWidget(self.param_tree, 0, 0, -1, 1)
        ui_layout.addLayout(select, 0, 1)
        ui_layout.addLayout(self.results, 1, 1)
        ui_layout.setColumnStretch(1, 2)
        self.setLayout(ui_layout)

    def set_data_preview(self, x, y):
        self.preview.clear()
        self.preview.showGrid(x=True, y=True, alpha=0.8)
        self.preview.plot(x, y, pen=(0, 0, 255))

    def set_data_serie(
        self, x, y, yunb=None, bmin1s=None, bmax1s=None, bmin2s=None, bmax2s=None
    ):
        """TODO handle table setData when data (bmin or bmax, yunb) are None"""
        table_data = [x, y]
        table_header = ["\u03C4 (s)", "\u03C3y"]
        if yunb is not None:
            table_data.append(yunb)
            table_header.append("\u03C3y unbiased")
        if bmin1s is None or bmax1s is None:
            top1s = None
            bottom1s = None
        else:
            top1s = bmax1s - y
            bottom1s = y - bmin1s
            table_data.append(bmin1s)
            table_data.append(bmax1s)
            table_header.append("Min \u03C3")
            table_header.append("Max \u03C3")
        if bmin2s is None or bmax2s is None:
            top2s = None
            bottom2s = None
        else:
            top2s = bmax2s - y
            bottom2s = y - bmin2s
            table_data.append(bmin2s)
            table_data.append(bmax2s)
            table_header.append("Min 2\u03C3")
            table_header.append("Max 2\u03C3")
        self.plot.plot_dev(
            x=x,
            y=y,
            yunb=yunb,
            top1s=top1s,
            bottom1s=bottom1s,
            top2s=top2s,
            bottom2s=bottom2s,
        )

        self.table.setData(np.transpose(table_data))
        self.table.setHorizontalHeaderLabels(table_header)
