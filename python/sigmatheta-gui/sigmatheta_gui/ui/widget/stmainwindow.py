# -*- coding: utf-8 -*-

"""package sigmatheta-gui
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     UI part of Sigma Theta program.
"""

import ntpath
from importlib.resources import files, as_file
from PyQt5 import Qt, QtCore, QtGui, QtWidgets
from sigmatheta_gui.ui.dialog import driremdialog
# from sigmatheta_gui.ui.dialog import psddialog
from sigmatheta_gui.ui.dialog import scalingdialog
from sigmatheta_gui.ui.dialog import x2ydialog
import sigmatheta_gui.data as stg_data
import sigmatheta_gui.ui.widget.seriewidget as stg_serie
import sigmatheta_gui.constants as csts
from sigmatheta_gui.version import __version__


class StMainWindow(QtWidgets.QMainWindow):
    """StMainWindow class, main UI of Sigma Theta program.
    """

    file_droped = QtCore.pyqtSignal(str)

    def __init__(self):
        """Constructor.
        """
        super().__init__()
        self.setWindowTitle(csts.APP_NAME)
        self._create_actions()
        # Lays out
        self._menu_bar = self.menuBar()
        self._populate_menubar()
        self.tool_bar = self.addToolBar("Tool Bar")
        self._populate_toolbar()
        self.tool_bar.setMovable(True)
        self.tool_bar.setFloatable(False)
        self.status_bar = self.statusBar()
        self.serie_tab = QtWidgets.QTabWidget()
        self.serie_tab.addTab(stg_serie.SerieWidget(csts.PLOT_LEGEND_FORMAT), "New")
        self.setCentralWidget(self.serie_tab)
        self.setAcceptDrops(True)
        # UI specific logic
        self._actions_logic()
        # Init widget display
        self.reset()

    def _create_actions(self):
        """Creates actions (used with bar widgets).
        :returns: None
        """
        self.action_import = QtWidgets.QAction(QtGui.QIcon.fromTheme("document-open"),
                                               "&Import", self)
        self.action_import.setStatusTip("Import text file")
        self.action_import.setShortcut('Ctrl+I')
        #
        self.action_new = QtWidgets.QAction(QtGui.QIcon.fromTheme("document-new"),
                                            "&New", self)
        self.action_new.setStatusTip("New serie analysis")
        self.action_new.setShortcut('Ctrl+N')
        #
        with as_file(files(stg_data).joinpath('tch-32px.png')) as p:
            self.action_new_tch = QtWidgets.QAction(QtGui.QIcon(str(p)),
                                                    "&Three cornered hat", self)
        self.action_new_tch.setStatusTip("New three cornered-hat analysis")
        self.action_new_tch.setShortcut('Ctrl+T')
        #
        """self.action_save = QAction(QIcon.fromTheme("document-save"),
                                   ("&Save"), self)
        self.action_save.setStatusTip("Save data")
        self.action_save.setShortcut('Ctrl+S')"""
        #
        self.action_quit = QtWidgets.QAction(QtGui.QIcon.fromTheme("application-exit"),
                                             "&Quit", self)
        self.action_quit.setStatusTip("Exit application")
        self.action_quit.setShortcut('Ctrl+Q')
        #
        self.action_run = QtWidgets.QAction(QtGui.QIcon.fromTheme("system-run"),
                                            "&Run", self)
        self.action_run.setStatusTip("Compute deviation serie")
        self.action_run.setShortcut('Ctrl+R')
        #
        """self.action_pref = QAction(QIcon.fromTheme("preferences-system"),
                                   "Preferences", self)
        self.action_pref.setStatusTip("Open preference dialog form")"""
        #
        with as_file(files(stg_data).joinpath('x2y-32px.png')) as p:
            self.action_x2y = QtWidgets.QAction(QtGui.QIcon(str(p)),
                                                "X2Y", self)
        self.action_x2y.setStatusTip("Phase to frequency conversion")
        #
        with as_file(files(stg_data).joinpath('scale-32px.png')) as p:
            self.action_scale = QtWidgets.QAction(QtGui.QIcon(str(p)),
                                                  "Scale", self)
        self.action_scale.setStatusTip("Scale data")
        #
        with as_file(files(stg_data).joinpath('normalize-32px.png')) as p:
            self.action_normalize = QtWidgets.QAction(QtGui.QIcon(str(p)),
                                                      "Normalize", self)
        self.action_normalize.setStatusTip("Normalize data")
        #
        with as_file(files(stg_data).joinpath('dri-32px.png')) as p:
            self.action_drirem = QtWidgets.QAction(QtGui.QIcon(str(p)),
                                                   "Drift remove", self)
        self.action_drirem.setStatusTip("Remove the linear drift")
        #
        with as_file(files(stg_data).joinpath('psd-32px.png')) as p:
            self.action_psd = QtWidgets.QAction(QtGui.QIcon(str(p)),
                                                "PSD", self)
        self.action_psd.setStatusTip("Frequency analysis")
        #
        self.action_about = QtWidgets.QAction(QtGui.QIcon.fromTheme("help-about"),
                                              f"About {csts.APP_NAME}", self)
        self.action_about.setStatusTip("Open about dialog form")

    def _actions_logic(self):
        self.action_psd.triggered.connect(self.on_action_psd)
        self.action_run.triggered.connect(self.on_action_run)

    def _populate_menubar(self):
        """Populates the menubar of the UI
        :returns: None
        """
        self._menu_bar.menu_file = self._menu_bar.addMenu("&File")
        self._menu_bar.menu_file.addAction(self.action_import)
        self._menu_bar.menu_file.addSeparator()
        self._menu_bar.menu_file.addAction(self.action_new)
        ##self._menu_bar.menu_file.addAction(self.action_new_tch)
        self._menu_bar.menu_file.addSeparator()
        self._menu_bar.menu_file.addAction(self.action_quit)
        self._menu_bar.menu_process = self._menu_bar.addMenu("&Process")
        self._menu_bar.menu_process.addAction(self.action_run)
        self._menu_bar.menu_process.addAction(self.action_x2y)
        self._menu_bar.menu_process.addAction(self.action_normalize)
        self._menu_bar.menu_process.addAction(self.action_scale)
        self._menu_bar.menu_process.addAction(self.action_drirem)
        self._menu_bar.menu_process.addAction(self.action_psd)
        self._menu_bar.menu_help = self._menu_bar.addMenu("&Help")
        self._menu_bar.menu_help.addAction(self.action_about)

    def _populate_toolbar(self):
        """Populates the toolbar of the UI
        :returns: None
        """
        self.tool_bar.addAction(self.action_new)
        self.tool_bar.addAction(self.action_import)
        self.tool_bar.addAction(self.action_x2y)
        self.tool_bar.addAction(self.action_run)
        self.tool_bar.addAction(self.action_normalize)
        self.tool_bar.addAction(self.action_scale)
        self.tool_bar.addAction(self.action_drirem)
        self.tool_bar.addAction(self.action_psd)
        ##self.tool_bar.addAction(self.action_new_tch)

    @QtCore.pyqtSlot()
    def reset(self):
        """Reset UI. This is the starting state when no data file is imported,
        functions regarding data processing are not accessible.
        :returns: None
        """
        self.action_import.setEnabled(True)
        self.action_run.setEnabled(False)
        self.action_x2y.setEnabled(False)
        self.action_new.setEnabled(False)
        self.action_normalize.setEnabled(False)
        self.action_scale.setEnabled(False)
        self.action_drirem.setEnabled(False)
        self.action_psd.setEnabled(False)
        self.action_new_tch.setEnabled(False)

    @QtCore.pyqtSlot(bool)
    def computing_state(self, mode=True):
        """Set UI in computing mode. This the "normal" state, a data file is
        imported and all the functions need to be accessible.
        :returns: None
        """
        self.action_import.setEnabled(True)
        self.action_run.setEnabled(mode)
        self.action_x2y.setEnabled(mode)
        self.action_new.setEnabled(mode)
        self.action_normalize.setEnabled(mode)
        self.action_scale.setEnabled(mode)
        self.action_drirem.setEnabled(mode)
        self.action_psd.setEnabled(mode)
        self.action_new_tch.setEnabled(mode)

    def on_action_run(self):
        """When run requested: set convenient UI for dev analysis.
        :returns: None
        """
        self.serie_tab.currentWidget().plot_rbtn.setChecked(True)
        self.serie_tab.currentWidget().results.setCurrentWidget(
            self.serie_tab.currentWidget().plot)

    def on_action_psd(self):
        """When psd requested: set convenient UI for frequency analysis.
        - Add a specific items in parameter tree
        - Add a plot dedicated to display result of frequency analysis
        :returns: None
        """
        self.serie_tab.currentWidget().psd_rbtn.setVisible(True)
        self.serie_tab.currentWidget().psd_rbtn.setChecked(True)
        self.serie_tab.currentWidget().results.setCurrentWidget(
            self.serie_tab.currentWidget().psd)

    def on_action_x2y(self):
        """When x2y requested: open a parameters dialog box and get
        the choosen parameter values.
        :returns: scale factors (xscale, yscale) (float, float)
        """
        dialog = x2ydialog.X2yDialog()
        dialog.setParent(None, Qt.Qt.Dialog)
        retval = dialog.exec_()
        if retval == QtWidgets.QDialog.Accepted:
            return dialog.xscale, dialog.yscale
        return -1

    def on_action_scale(self):
        """When scale requested: open a parameters dialog box and get
        the choosen parameter values.
        :returns: scale factors (offset, multiplier) (float, float)
        """
        dialog = scalingdialog.ScalingDialog()
        dialog.setParent(None, Qt.Qt.Dialog)
        retval = dialog.exec_()
        if retval == QtWidgets.QDialog.Accepted:
            return dialog.scale, dialog.offset
        return -1

    def on_action_drirem(self):
        """When drirem requested: open a parameters dialog box and get
        the choosen parameter values.
        :returns: Drift remove order (int)
        """
        dialog = driremdialog.DriRemDialog()
        dialog.setParent(None, Qt.Qt.Dialog)
        retval = dialog.exec_()
        if retval == QtWidgets.QDialog.Accepted:
            return dialog.drift_order
        return -1

    def deviance_type(self):
        return self.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Deviance type').value()

    def tau_step(self):
        return self.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Tau step').value()

    def tau(self):
        return self.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Integration time (s)').value()

    def set_tau(self, value):
        self.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'tau').setValue(value)

    def set_filename(self, value):
        self.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Filename'). \
            setValue(ntpath.basename(value))
        self.serie_tab.setTabText(self.serie_tab.currentIndex(), value)

    # The following three methods set up dragging and dropping for the app
    def dragEnterEvent(self, e):
        if e.mimeData().hasUrls:
            e.accept()
        else:
            e.ignore()

    def dragMoveEvent(self, e):
        if e.mimeData().hasUrls:
            e.accept()
        else:
            e.ignore()

    def dropEvent(self, e):
        """Drop files directly onto the widget.
        File locations are stored in fname.
        :param e:
        :return:
        """
        if e.mimeData().hasUrls:
            e.setDropAction(QtCore.Qt.CopyAction)
            e.accept()
            # Workaround for OSx dragging and dropping
            for url in e.mimeData().urls():
                fname = str(url.toLocalFile())
            self.file_droped.emit(fname)
        else:
            e.ignore()


#==============================================================================
if __name__ == "__main__":
    """Check UI display.
    """

    import sys
    import numpy as np

    X = np.arange(1,10)
    Y = 2E-10*np.arange(1,10)
    DY = 1E-10*np.arange(1,10)
    DY2 = DY * 2
    COEFF = [0, 2.174317e-25, 9.832043e-26, 2.319278e-27, \
             1.501548e-30, 3.418445e-35]

    print(X, '\n', Y, '\n', DY)

    APP = QtWidgets.QApplication(sys.argv)
    UI = StMainWindow()

    UI.serie_tab.currentWidget().set_data_serie(x=X, y=Y,
                                                bmin1s=DY, bmax1s=DY,
                                                bmin2s=DY2, bmax2s=DY2)
    UI.serie_tab.currentWidget().plot.plot_asymptotes(COEFF, True)
    UI.show()
    sys.exit(APP.exec_())
