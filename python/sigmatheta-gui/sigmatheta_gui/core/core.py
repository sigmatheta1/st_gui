# -*- coding: utf-8 -*-

"""package sigmatheta
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Data processing functions of Sigma Theta package.
"""

import logging
import st_binding as stb
import st_binding.core as stbc

# import scipy.signal as scs
import numpy as np
import numpy.polynomial.polynomial as poly


SCALE = {
    "d": 86400,
    "H": 3600,
    "M": 60,
    "m": 1.0e-3,
    "u": 1.0e-6,
    "n": 1.0e-9,
    "p": 1.0e-12,
}

DEV_SLOPE = {
    "ADEV": np.array([-2, -1, 0, 1, 2]),
    "MDEV": np.array([-3, -2, -1, 0, 1, 2]),
    "HDEV": np.array([-1, 0, 1, 2, 3]),
    "PDEV": np.array([-3, -2, -1, 0, 1, 2]),
    "GDEV": np.array([-3, -2, -1, 0, 1, 2]),
    "HCDEV": np.array([-3, -2, -1, 0, 1, 2]),
}

FLAG_SLOPE = {
    "ADEV": np.array([0, 1, 1, 1, 1, 1]),
    "MDEV": np.array([1, 1, 1, 1, 1, 1]),
    "HDEV": np.array([0, 0, 1, 1, 1, 1]),
    "PDEV": np.array([1, 1, 1, 1, 1, 1]),
    "GDEV": np.array([1, 1, 1, 1, 1, 1]),
    "HCDEV": np.array([1, 1, 1, 1, 1, 1]),
}


# =============================================================================
def process_dev(tau_base, y, deviation_type, tau_inc_type, pattern=[0]):
    """Process deviation: compute frequency instability.
    :param tau_base: Integration time in second (int)
    :param y: a 1D array containing temporal data (numpy.array)
    :param deviation_type: index (see st_binding.py) representing deviation
                           type to use (int)
    :param ortau: Integration time increment structure (object)
    :returns: result of deviation processing (object)
    """
    flag_bias = True  # Unbiased estimate flag, TODO: use config file(?)
    flag_variance = False
    flag_slopes = FLAG_SLOPE[deviation_type]
    #
    serie = stbc.st_serie()
    n = len(y)
    deviation_type = stb.DEVIANCE_TYPE[deviation_type]
    tau_inc_type = stb.TAU_INC_TYPE[tau_inc_type]
    retval = stbc.populate_tau_list(
        serie, n, deviation_type, tau_base, tau_inc_type, pattern
    )
    if retval != 0:
        logging.error("Tau list generation failed: %d", retval)
        return retval
    retval = stbc.st_serie_dev(serie, deviation_type, n, y)
    if retval != 0:
        logging.error("Frequency instability computation failed: %d", retval)
        return retval
    #
    stbc.st_relatfit(serie, serie.tau, 6, flag_slopes, flag_bias)
    #
    stbc.st_asym2alpha(serie, flag_variance)
    #
    edf = np.empty(
        [
            128,
        ],
        dtype=float,
    )
    stbc.st_avardof(serie, edf, flag_variance)
    #
    stbc.st_aduncert(serie, edf)
    #
    weight = np.empty(
        [
            serie.length,
        ],
        dtype=float,
    )
    weight = 1 / edf[: serie.length]
    err = stbc.st_relatfit(serie, weight, 6, flag_slopes, flag_bias)
    #
    return serie


# =============================================================================
def drift_remove(t_data, f_data, order, filter_=False):
    """Remove drift: output_data = data - polynomial_fit_of_data
    Compute the least-squares fit of a polynomial to input data then supress
    the polynomial to these data.
    :param t_data: a 1D array of timetag serie (numpy.array)
    :param f_data: a 1D array of frequency serie  (numpy.array)
    :param order: order of the polynomial (int)
    :param filter: flag handling post-process filtering of data (bool)
    :returns: "de-drifted" data, fit coefficients (numpy.array, numpy.array)
    """
    # Suppress mean time to get better fit:
    t_data = t_data - t_data.mean()
    try:
        coeffs = poly.polyfit(t_data, f_data, order)
    except Exception as ex:
        logging.error("Exception: %r", ex)
        raise ex
    yfit = poly.polyval(t_data, coeffs)
    f_data = f_data - yfit
    # if filter_ is True:
    #     b, a = scs.butter(3, 0.005)
    #     ##res = scs.filtfilt(b, a, f_data, padlen=150)
    return f_data, coeffs


# =============================================================================
def normalize(data):
    """Normalize data: output_data = data - mean(data)
    :param data: a 1D array of data to normalize (numpy.array)
    :returns: data normalized (numpy.array)
    """
    return data - np.mean(data)


# =============================================================================
def scale(data, m_factor=1.0, s_factor=0.0):
    """Scale data: output_data = data * m_factor + s_factor
    :param data: a 1D array of data to scale (numpy.array)
    :param m_factor: multiplier factor (float)
    :param s_factor: sum factor (float)
    :returns: data scaled
    """
    return data * m_factor + s_factor


# =============================================================================
def x2y(xdata, tau=1, xscale=1):
    """Convert phase serie to frequency fluctuation serie.
    :param xdata: a 1D array of phase value (numpy.array)
    :returns: data converted to frequency fluctuation (numpy.array)
    """
    ydata = np.diff(xdata) / tau / xscale
    return ydata


# =============================================================================
def psd(y, tau_step, ny, dec):
    """Power Spectral Density (PSD) calculation.
    Compute the Power Spectral Density of the 'ny' normalized
    frequency deviation elements of the vector 'y'  frequency deviation)
    versus the Fourrier frequency.
    :param y: Array of normalized frequency deviation samples

    :param tau_step: Array of timetag data

    :param ny: Number of elements of samples used in the computation
    :param dec: Decimation flag default False (boolean)
    :returns: Arrays of Fourrier frequency values and PSD values
              (np.array of float, np.array of float)
    """
    ff, syy = stbc.st_psdgraph(y, tau_step, ny, dec)
    return ff, syy


# =============================================================================
def process_tch(sab, sbc, sca):
    """Computes individual instability of 3 sources A, B and C using the Three
    Cornered Hat algorithm.
    :param sab: deviation serie of sources A and B (array)
    :param sbc: deviation serie of sources B and C (array)
    :param sca: deviation serie of sources C and A (array)
    :returns: individual deviation serie of sources A, B and C (array, array, array)
    """
    # TODO: check behavior of np.sqrt(x) when x < 0 (result is a complex)
    sa = np.sqrt(np.pow(sab, 2) + np.pow(sca, 2) - np.pow(sbc, 2))
    sb = np.sqrt(np.pow(sbc, 2) + np.pow(sab, 2) - np.pow(sca, 2))
    sc = np.sqrt(np.pow(sca, 2) + np.pow(sbc, 2) - np.pow(sab, 2))
    return sa, sb, sc
