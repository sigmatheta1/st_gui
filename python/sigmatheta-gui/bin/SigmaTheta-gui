#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""package sigmatheta-gui
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Main file of SigmaTheta-gui program
"""

# For "Ctrl+C" works
import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

import sys
import logging
import os.path as path
import PyQt5.QtWidgets as QtWidgets
import sigmatheta_gui.ui.st_gui as st_gui
import sigmatheta_gui.constants as csts

CONSOLE_LOG_LEVEL = logging.DEBUG
FILE_LOG_LEVEL = logging.DEBUG


#==============================================================================
def configure_logging():
    """Configures logs.
    """
    home = path.expanduser("~")
    log_file = "." + csts.APP_NAME + ".log"
    abs_log_file = path.join(home, log_file)
    date_fmt = "%d/%m/%Y %H:%M:%S"
    log_format = "%(asctime)s %(levelname) -8s %(filename)s " + \
                 " %(funcName)s (%(lineno)d): %(message)s"
    logging.basicConfig(level=FILE_LOG_LEVEL,
                        datefmt=date_fmt,
                        format=log_format,
                        filename=abs_log_file,
                        filemode='w')
    console = logging.StreamHandler()
    # define a Handler which writes messages to the sys.stderr
    console.setLevel(CONSOLE_LOG_LEVEL)
    # set a format which is simpler for console use
    console_format = '%(levelname) -8s %(filename)s (%(lineno)d): %(message)s'
    formatter = logging.Formatter(console_format)
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)



#==============================================================================
configure_logging()
app = QtWidgets.QApplication(sys.argv)
ui = st_gui.StGui()
sys.exit(app.exec_())