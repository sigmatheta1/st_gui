==============
sigmatheta-gui
==============

INTRODUCTION
============

sigmatheta-gui proposes a graphical interface over the Sigma Theta library.
The Sigma Theta library is a collection of numerical programs for time and frequency metrology. More details on the webpage of the project:
https://gitlab.com/fm-ltfb/SigmaTheta


REQUIREMENTS
============

Preferably a Linux system with a Debian distribution (not tested with others OS
nor others distributions).
The program requires the Python 3 (Not tested with Python 2) and pip installed.

* pip , on a Debian system:
      $ [sudo] apt-get install python3-pip


INSTALLATION
============

BASIC USER INSTALL
------------------

      $ python3 -m pip --user sigmatheta-gui

Check that "~/.local/bin" is in your PATH. If not, on Linux, you can add this to your profile file using the following command:

      $ echo "export PATH=/home/${USER}/.local/bin:$PATH" >> ~/.profile


VENV INSTALL
------------

Create a venv if you don't have one ready, for example:

      $ python3 -m venv ~/.venv_st

This create a venv in the directory '~/.venv_st'

Activate the venv:

      $ source ~/.venv_st/bin/activate

This should add the name of the venv in your prompt, like this:

      (.venv_st) $

Now instal the package

      (.venv_st) $ python3 -m pip --user sigmatheta-gui


USAGE
=====

BASIC USER INSTALL
------------------
Simply call the software:

      $ SigmaTheta-gui


VENV INSTALL
------------
Activate the venv before using the software:

      $ source ~/.venv_st/bin/activate

Then call the software:

      (.venv_st) $ SigmaTheta-gui
